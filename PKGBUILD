# Maintainer: JesusOfSuburbia <jesusofsuburbia@tuta.io>
# Upstream maintainer: lah7
# Upstream maintainer: jonathon
pkgname=gtk3-classic
pkgver=3.24.42
pkgrel=1
pkgdesc="GTK3 patched for classic desktops, like Xfce or MATE"
url="https://github.com/lah7/gtk3-classic"
conflicts=(gtk3 gtk3-typeahead gtk3-print-backends)
provides=(gtk3=${pkgver} gtk3-typeahead=${pkgver} gtk3-mushrooms=${pkgver} gtk3-print-backends
          libgtk-3.so libgdk-3.so libgailutil-3.so)
replaces=("gtk3-print-backends<=3.22.26-1")
arch=(x86_64)
license=(LGPL-2.1-or-later)
makedepends=(
	gobject-introspection libcanberra gtk-doc sassc libcups meson quilt atk
	cairo libxcursor libxinerama libxrandr libxi libepoxy gdk-pixbuf2 fribidi
	libxcomposite libxdamage pango shared-mime-info at-spi2-atk libxkbcommon
	json-glib librsvg desktop-file-utils mesa gtk-update-icon-cache wayland
	adwaita-icon-theme cantarell-fonts wayland-protocols libegl libgl colord
	)
depends=(
	atk cairo libxcursor libxinerama libxrandr libxi libepoxy gdk-pixbuf2 fribidi
	libxcomposite libxdamage pango shared-mime-info at-spi2-atk libxkbcommon librsvg
	json-glib desktop-file-utils mesa gtk-update-icon-cache
	)
optdepends=(
	'libcups: printers in printing dialog'
	'dconf: default GSettings backend'
	'libcanberra: sounds events'
	'gnome-icon-theme: default icon theme'
	'gnome-icon-theme-extras: additional icons'
	'gnome-icon-theme-symbolic: symbolic icons'
	'gtk3-docs: documentation'
	'gtk3-demos: demo applications'
	'xreader: default print preview command'
	'wayland: wayland backend'
	'colord: color profile support'
	)
install=gtk3.install
source=(
	series
	appearance__buttons-menus-icons.patch
	appearance__disable-backdrop.patch
	appearance__file-chooser.patch
	appearance__message-dialogs.patch
	appearance__print-dialog.patch
	appearance__smaller-statusbar.patch
	csd__clean-headerbar.patch
	csd__disabled-by-default.patch
	csd__server-side-shadow.patch
	file-chooser__eject-button.patch
	file-chooser__places-sidebar.patch
	file-chooser__typeahead.patch
	fixes__labels-wrapping.patch
	fixes__primary_selection.patch
	other__default-settings.patch
	other__hide-insert-emoji.patch
	other__mnemonics-delay.patch
	other__remove_dead_keys_underline.patch
	popovers__color-chooser.patch
	popovers__file-chooser-list.patch
	popovers__places-sidebar.patch
	notebook__wheel_scroll.patch
	treeview__alternating_row_colors.patch
	window__rgba-visual.patch
	smaller-adwaita.css
	"git+https://gitlab.gnome.org/GNOME/gtk.git#tag=${pkgver}"
	settings.ini
	gtk-query-immodules-3.0.hook
	)
sha256sums=('4add3f24e32ed3d1fd360da735bed2f9caab964737749374f034f20aaf94da8e'
            '7d2d776b9c3199e68d8ebaca1947966670a90efc4857f68d8fe2e6d20d86e585'
            'd132b72777c00ffc22a7cf33b0a1cc11de2f8a67ea75bb02c16385d30220612c'
            '7332e779cadf408680ad965d1486f8df0bd8510208b46888ec6e38b01ccb12f6'
            'e13ba6c543a2e48eff9b074751180e559d12ea48647b1a5687103aa9f619345d'
            '0672f31a61b1b2f8555fbdde267db1585e395e5e29bdea9b3fe7b08b7b568f32'
            '24217b43a7ca5bd46ff205b8f2a7c5a5192cafc36f5093255ed9053e5496afed'
            'c9155a3525608b46715b29bffe667876dfd5b72d9f5baf4158bdfefb447568a1'
            '371fa6604f872ecaef421d68a85d8bab4908c7d6413c60056a80623644c1af13'
            'e92ab6987da9c41e409386b5b5fd8ddf361a3991f87cc7ffcfd90e60f1375554'
            'decb40fa599fe2b2a5995d4c03f93776192c037794801a98125593a2467f3177'
            'ad25e4d31324c51c836b4c945af97b2bf7f673d1be58c584f7188d3d82a75530'
            '6f786d00fb2a0f99a0fedb6efa146eed0a5bdf25042424d3a182227112059e60'
            '2c4b1dc816bdb0c0e3e5e6d6b40908f0ba820505f9cdb33d38dca61b2067d019'
            '135defcbaa4832ae09c79d39231f327c1399159c1a7520c2ebfd2ca5d7fc9a7b'
            'eeffb5341e9218018a54feb471a027e1ed4af728605b09a38b027570de0d3a9d'
            'afa69994ca4d2695cd2f01cc7bd70036f5d04822ae87e1496b9a3eb34b9c2c46'
            '40ca07fffcac8da7bb85a1de4aebdf84171b271b2735db349ed003ee3e83a60c'
            '30a3da34147fb114d9f7d6e4dc7c8f89d1dc0cecceaa8fd61e0d7f41397a8cba'
            '0414bbdc1197fcd9f8bae7dbf19c735df6168a66df699e5cb7e8ded5faa549b6'
            'f172d4a76fc7a201cb135aa2ae56e30689d293fe8ba79939663ba65f9898af83'
            '4b83a06abe98b9502cfd1243bac0563cd664ae63b7f2449116632c8a4dbb0a4a'
            'b81179f037d0531b92e2a4ccbab69768d6a81f3f32705c8328a16660738165af'
            '6579148f62974c225377c6f34d19520491d2a14006556203c214935e9855d8f4'
            '4ef50460216899f5572879e6935f6d9758347de3e8cedd1d95ca1ba6789eb817'
            'ba93f62e249f2713dbfe6c82de1be4ac655264d6407ed3dc5e05323027520f31'
            '38cbd3c15a9f4163f514ba40a7e4d0f97f32935afad4a1227d66d5aee3c580dc'
            '1518ffacf74d2a51574524af16909bc5e26df1633cfecb2b062865a72e77e270'
            'a0319b6795410f06d38de1e8695a9bf9636ff2169f40701671580e60a108e229')

prepare() {
	cd gtk
	QUILT_PATCHES=.. quilt push -av

	rm -f "${srcdir}"/gtk/gtk/theme/Adwaita/gtk-contained{,-dark}.css
	cat "${srcdir}/smaller-adwaita.css" | tee -a "${srcdir}"/gtk/gtk/theme/Adwaita/gtk-contained{,-dark}.css > /dev/null
}

build() {
	local meson_options=(
		-D broadway_backend=false
		-D wayland_backend=true
		-D cloudproviders=false
		-D colord=yes
		-D demos=false
		-D examples=false
		-D gtk_doc=false
		-D man=true
		-D tests=false
		-D installed_tests=false
		-D tracker3=false
	)
	CFLAGS+=" -DG_DISABLE_CAST_CHECKS"
	arch-meson --buildtype minsize gtk build "${meson_options[@]}"
	meson compile -C build
}

package() {

	meson install -C build --destdir "${pkgdir}"
	install -Dt "${pkgdir}/usr/share/gtk-3.0" -m644 settings.ini
	install -Dt "${pkgdir}/usr/share/libalpm/hooks" -m644 gtk-query-immodules-3.0.hook
	rm "${pkgdir}/usr/bin/gtk-update-icon-cache"
	rm "${pkgdir}/usr/share/man/man1/gtk-update-icon-cache.1"
}
