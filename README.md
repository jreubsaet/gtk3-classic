# GTK 3 Classic
This is a set of patches for GTK 3, maintained upstream by [lah7](https://github.com/lah7/gtk3-classic), including modifications to **settings.ini**. Most work done is changing the line numbers to keep up with the upstream changes to GTK 3. If further intervention were required, this would have to be looked at when the time is due.
This package is based on Arch Linux's upstream GTK 3 package.

However, there is one major, notable difference: the references to icons in buttons, and menus have been changed to no longer depend on gtk-<item> type names (which are symlinks in most icon themes anyway), but instead refer to the icons which the symlinks resolve to.  This 'fixes' the issue of possibly missing icons in menus, if the currently selected icon theme does not include these symlinks.

As a pre-caution, this same substitution was applied to the GTK dialogs, which now reference their icons without gtk<name> prefixes too.

## Note
Disabling the Wayland backend *at all* breaks Xfce 4 (and possibly MATE and Cinnamon) on Arch Linux (which complains about missing symbols if it is compiled without it).
